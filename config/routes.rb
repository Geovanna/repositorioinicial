Rails.application.routes.draw do
  resources :admins
  resources :posts
  devise_for :users
  root 'posts#index'
  
end
